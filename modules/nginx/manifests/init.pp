class nginx {
  package { 'nginx' :
    name   => 'nginx',
    ensure => '1.4.6-1ubuntu3.3',
  }

  # to do - use augeas for configs
  file { '/etc/nginx/sites-available/api' :
    source  => 'puppet:///modules/nginx/api.conf',
    mode    => 644,
    owner   => 'root',
    notify  => Service['nginx'],
    require => Package['nginx'],
  }

  file { '/etc/nginx/sites-enabled/api' :
    ensure  => 'link',
    target  => '/etc/nginx/sites-available/api',
    require => File['/etc/nginx/sites-available/api'],
  }

  file { '/home/vagrant/log':
    ensure => 'directory',
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0644',
  }

  file { '/home/vagrant/log/cache':
    ensure => 'directory',
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0644',
  }

  service { 'apache2' :
    ensure => 'stopped',
  }

  service { 'nginx' :
      ensure  => 'running',
      enable  => 'true',
      require => [Package['nginx'], Service['apache2']],
  }
}
