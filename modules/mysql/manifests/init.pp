class mysql {
  package { 'mysql-server' :
    name   => 'mysql-server',
    ensure => '5.5.46-0ubuntu0.14.04.2',
  }

  service { 'mysql' :
    ensure  => 'running',
    enable  => 'true',
    require => Package['mysql-server'],
  }
}
