class php {
  package { 'php5' :
    name   => 'php5',
    ensure => '5.5.9+dfsg-1ubuntu4.14',
  }

  package { 'php5-mcrypt' :
    name   => 'php5-mcrypt',
    ensure => '5.4.6-0ubuntu5',
  }

  package { 'php5-memcache' :
    name   => 'php5-memcache',
    ensure => '3.0.8-4build1',
  }

  package { 'php5-curl' :
    name   => 'php5-curl',
    ensure => '5.5.9+dfsg-1ubuntu4.14',
  }

  package { 'php5-fpm' :
    name   => 'php5-fpm',
    ensure => '5.5.9+dfsg-1ubuntu4.14',
  }

  # to do - use augeas for configs
  file { '/etc/php5/fpm/php.ini' :
    source => 'puppet:///modules/php/php.ini',
    mode => 644,
    owner => 'root',
    notify => Service['php5-fpm'],
    require => Package['php5-fpm'],
  }

  file { '/etc/php5/fpm/pool.d/www.conf' :
    source => 'puppet:///modules/php/www.conf',
    mode => 644,
    owner => 'root',
    notify => Service['php5-fpm'],
    require => Package['php5-fpm'],
  }

  service { 'php5-fpm' :
      ensure  => 'running',
      enable  => 'true',
      require => Package['php5-fpm'],
  }
}
